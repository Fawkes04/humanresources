import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
//import java.util.List;

public class HumanResources {
    ArrayList<Staff> myEmployees = new ArrayList<>();
    HashMap<String, Integer> nameIdDep = new HashMap<>();
    ArrayList<Department> detailDep = new ArrayList<>();
    int sl = 0; 
    
    public static void main(String[] args) {
        
        HumanResources sources = new HumanResources();
        Scanner sc = new Scanner(System.in);
       

        while(true){
            System.out.println("\n-------------------------");
            System.out.println("1. Show staffs list");
            System.out.println("2. Show departments");
            System.out.println("3. Show staffs in each departments");
            System.out.println("4. Add a new staff");
            System.out.println("5. Search a staff by name or ID");
            System.out.println("6. Show staffs salary");
            System.out.println("7. Show staffs salary in increasing order");
            System.out.println("8. Exit");
            System.out.print("Your choice: ");
            
            int choice = sc.nextInt();
            System.out.println();

             switch(choice){
                 case 2:
                    sources.displayDepart();
                    break;
                 case 3:
                    sources.displayStaffDepart();
                    break;
                 case 4:
                    sources.adding();
                    break;
                 case 5:
                    sources.search();
                    break;
                 case 8:
                    System.exit(0);
             }
        }
    }

    //Them nhan vien vao cong ty
    public void adding(){
        System.out.println("1. Add an employee");
        System.out.println("2. Add a manager");
        
        Scanner sc = new Scanner(System.in);
        int choice2 = sc.nextInt();   sc.nextLine();

        System.out.println("Enter information for a new staff:");
        
        System.out.print("ID: ");
        String MaNhanVien = sc.nextLine(); MaNhanVien = MaNhanVien.toUpperCase();

        System.out.print("name: ");
        String TenNhanVien = sc.nextLine();

        System.out.print("age: ");
        int Tuoi = sc.nextInt();  sc.nextLine();

        System.out.print("coefficient pay: ");
        double HSLuong = sc.nextDouble();

        System.out.print("day start working: ");
        int NgayVaoLam = sc.nextInt();  sc.nextLine();

        System.out.print("department: ");
        String BoPhan = sc.nextLine();  
        BoPhan = BoPhan.toLowerCase();

        //Adding departments ==============================================
        
        if(nameIdDep.get(BoPhan) == null){
            //Add them bo phan vao
           nameIdDep.put(BoPhan, sl);
           String MaBoPhan = "dep_00"+sl;
           Department dep = new Department(MaBoPhan, BoPhan, 1);
           detailDep.add(dep);
            //Tang so luong bo phan
           sl++;
           //System.out.println("if");
        }
        else{
            //Tang so luong nhan vien trong bo phan
            int slnv = detailDep.get(nameIdDep.get(BoPhan)).getSLNhanVien();
            detailDep.get(nameIdDep.get(BoPhan)).setSLNhanVien(slnv+1);
            //System.out.println("else");
        }
        //=================================================================

        System.out.print("number of off-days: ");
        int SoNgayNghi = sc.nextInt();  sc.nextLine();

        if(choice2 == 1){
            System.out.print("extra working hours: ");
            int GioLamThem = sc.nextInt(); 
            Staff em = new Employee(MaNhanVien, TenNhanVien, BoPhan, Tuoi, NgayVaoLam, SoNgayNghi, HSLuong, GioLamThem);
            myEmployees.add(em);
            System.out.println("You successfully add a new employee!");
        }
        else{
            System.out.print("Title: ");
            String ChucDanh = sc.nextLine(); 
            Staff em = new Manager(MaNhanVien, TenNhanVien, BoPhan, Tuoi, NgayVaoLam, SoNgayNghi, HSLuong, ChucDanh);
            myEmployees.add(em);
            System.out.println("You successfully add a new manager!");
        }

    }

    //Hien thi cac bo phan 
    public void displayDepart(){
        if(detailDep.size()==0){
            System.out.println("There are no departments in company!");
            return;
        }
        System.out.println(String.format("%-20s%-20s%-20s", "Department_ID", "Department_name", "Department_staff"));
        for(Department dep: detailDep){
            System.out.println(dep.toString());
        }
    }

    //Hien thi cac nhan vien theo tung bo phan
    public void displayStaffDepart(){
        for(String i : nameIdDep.keySet()){
            System.out.println("Staffs in "+ i + " department: ");
            
            for(Staff j : myEmployees){
                if(j.getBoPhan().equals(i)){
                    System.out.println(j.toString());
                }
            }
        }
    }

    //Tiem kiem thong tin nhan vien
    public void search(){
        Scanner sc = new Scanner(System.in);
        String searchKey = "";
        System.out.println("Enter a ID or name of the staff.");
        System.out.print("ID/name: "); searchKey = sc.nextLine(); searchKey = searchKey.toLowerCase();

        ArrayList<Staff> emsFound = new ArrayList<>();
        for(Staff em: myEmployees){
            if(em.getTenNhanVien().toLowerCase().contains(searchKey) || em.getMaNhanVien().toLowerCase().contains(searchKey)){
                emsFound.add(em);
            }
        }

        if(emsFound.isEmpty()){
            System.out.println("No staff is found");
        }
        else{
            //System.out.println(String.format("%-10s%-20s%-20s%-20s", "ID", "Title", "Author", "Is borrowed"));
            for(Staff em:emsFound){
                System.out.println(em.toString());
            }
        }
    }

    //Sorting in salary
    public void displaySalary(){
        ArrayList<Staff> emSalary = myEmployees;
        Collections.sort(emSalary, new Comparator<Staff>(){
            @Override
            public int compare(Staff s1, Staff s2){
                if(s1.calculateSalary()<s2.calculateSalary()){
                    return 1;
                }
                else{
                    if(s1.calculateSalary() == s2.calculateSalary()){
                        return 0;
                    }
                    else{
                        return -1;
                    }
                }
            }
        });


    }
}
