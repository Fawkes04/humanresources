
public class Employee extends Staff{
    private int GioLamThem;
    
    public Employee(String MaNhanVien, String TenNhanVien, String BoPhan, int Tuoi, int NgayVaoLam, int SoNgayNghi, double HSLuong, int GioLamThem){
        super(MaNhanVien, TenNhanVien, BoPhan, Tuoi, NgayVaoLam, SoNgayNghi, HSLuong);
        this.GioLamThem = GioLamThem;
    }    
    public void setGioLamThem(int GioLamThem){
        this.GioLamThem = GioLamThem;
    }
    public int getGioLamThem(){
        return GioLamThem;
    }
    
    @Override
    // public void display(){
    //     System.out.println("Nhan vien ma: "+ super.getMaNhanVien());
    //     System.out.println("Nhan vien ten: "+ super.getTenNhanVien());
    //     System.out.println("Nhan vien tuoi: "+ super.getTuoi());
    //     System.out.println("Nhan vien he so luong: "+ super.getHSLuong());
    //     System.out.println("Nhan vien ngay vao lam : "+ super.getNgayVaoLam());
    //     System.out.println("Nhan vien bo phan lam viec: "+ super.getBoPhan());
    //     System.out.println("Nhan vien so ngay nghi phep: "+ super.getSoNgayNghi());
    // }

    public String toString(){// manv ten  tuoi hsluon vao  bphan nghi lamthem
        return String.format("%-10s%-20s%-10d%-10.2f%-10d%-10s%-10d%-10d", getMaNhanVien(), getTenNhanVien(), getTuoi(), getHSLuong(), getNgayVaoLam(), getBoPhan(), getSoNgayNghi(), getGioLamThem());
    }

    public double calculateSalary() {
		return getHSLuong()*3000000+ GioLamThem*200000;
	}

}