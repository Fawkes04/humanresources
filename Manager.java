public class Manager extends Staff {
    private String ChucDanh ;
    private int LuongTrachNhiem;
    
    public Manager(String MaNhanVien, String TenNhanVien, String BoPhan, int Tuoi, int NgayVaoLam, int SoNgayNghi, double HSLuong, String ChucDanh){
        super(MaNhanVien, TenNhanVien, BoPhan, Tuoi, NgayVaoLam, SoNgayNghi, HSLuong);
        this.ChucDanh = ChucDanh;
    }    
    public void setChucDanh(String ChucDanh){
        this.ChucDanh = ChucDanh;
    }
    public String getChucDanh(){
        return ChucDanh;
    }

    @Override
    // public void display(){
    //     System.out.println("Nhan vien ma: "+ super.getMaNhanVien());
    //     System.out.println("Nhan vien ten: "+ super.getTenNhanVien());
    //     System.out.println("Nhan vien tuoi: "+ super.getTuoi());
    //     System.out.println("Nhan vien he so luong: "+ super.getHSLuong());
    //     System.out.println("Nhan vien ngay vao lam day: "+ super.getNgayVaoLam());
    //     System.out.println("Nhan vien bo phan lam viec: "+ super.getBoPhan());
    //     System.out.println("Nhan vien so ngay nghi phep: "+ super.getSoNgayNghi());
    //     System.out.println("Nhan vien chuc danh: "+ ChucDanh);
    // }

    public String toString(){
        return String.format("%-10s%-20s%-10d%-10.2f%-10d%-10s%-10d%-10s", getMaNhanVien(), getTenNhanVien(), getTuoi(), getHSLuong(), getNgayVaoLam(), getBoPhan(), getSoNgayNghi(), getChucDanh());
    }
    
    public double calculateSalary() {
        if("Business Leader".equals(ChucDanh)){
            LuongTrachNhiem = 8000000;
        }
        else if("Project Leader".equals(ChucDanh)){
            LuongTrachNhiem = 5000000;
        }
        else{
            LuongTrachNhiem = 6000000;
        }
		return getHSLuong()*5000000 + LuongTrachNhiem;
	}
}
