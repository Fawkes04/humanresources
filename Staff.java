import java.util.Scanner;


public abstract class Staff implements ICaculator{
    private String MaNhanVien;
    private String TenNhanVien;
    private String BoPhan;
    private int Tuoi;
    private int NgayVaoLam;
    private int SoNgayNghi;
    private double HSLuong;
    
    public Staff(String MaNhanVien, String TenNhanVien, String BoPhan, int Tuoi, int NgayVaoLam, int SoNgayNghi, double HSLuong){
        this.MaNhanVien = MaNhanVien;
        this.TenNhanVien = TenNhanVien;
        this.BoPhan = BoPhan;
        this.Tuoi = Tuoi;
        this.NgayVaoLam = NgayVaoLam;
        this.SoNgayNghi = SoNgayNghi;
        this.HSLuong = HSLuong;
    }

    //maNhanVien, TenNhanVien, BoPhan
    public void setMaNhanVien(String MaNhanVien){
        this.MaNhanVien = MaNhanVien;
    }
    public String getMaNhanVien(){
        return MaNhanVien;
    }
    public void setBoPhan(String BoPhan){
        this.BoPhan = BoPhan;
    }
    public String getBoPhan(){
        return BoPhan;
    }
    public void setTenNhanVien(String TenNhanVien){
        this.TenNhanVien = TenNhanVien;
    }
    public String getTenNhanVien(){
        return TenNhanVien;
    }
    
    //Tuoi, NgayVaoLam, SoNgayNghi
    public void setTuoi(int Tuoi){
        this.Tuoi = Tuoi;
    }
    public int getTuoi(){
        return Tuoi;
    }
    public void setNgayVaoLam(int NgayVaoLam){
        this.NgayVaoLam = NgayVaoLam;
    }
    public int getNgayVaoLam(){
        return NgayVaoLam;
    }
    public void setSoNgayNghi(int SoNgayNghi){
        this.SoNgayNghi = SoNgayNghi;
    }
    public int getSoNgayNghi(){
        return SoNgayNghi;
    }

    //Hesoluong
    public void setHSLuong(double HSLuong){
        this.HSLuong = HSLuong;
    }
    public double getHSLuong(){
        return HSLuong;
    }    

    //toString abstract method
    public abstract String toString();
    
    public double compareTo(Staff compareObj){
        double compareSalary = compareObj.calculateSalary();
        return this.calculateSalary() - compareSalary;
    }
}